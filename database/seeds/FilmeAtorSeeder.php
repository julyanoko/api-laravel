<?php

use \App\Filme;
use \App\Ator;
use Illuminate\Database\Seeder;

class FilmeAtorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $filmesIds = Filme::all()->pluck('id')->toArray();
        $atoresIds = Ator::all()->pluck('id')->toArray();
        for ($x = 0; $x < 500; $x++) {
            DB::table('filmes_atores')->insert([
                'id_filme' => $filmesIds[array_rand($filmesIds)],
                'id_ator' => $atoresIds[array_rand($atoresIds)]
            ]);
        }
    }
}

<?php

use \App\Filme;
use \App\Diretor;
use Illuminate\Database\Seeder;

class FilmeDiretorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $filmesIds = Filme::all()->pluck('id')->toArray();
        $diretoresIds = Diretor::all()->pluck('id')->toArray();
        for ($x = 0; $x < 500; $x++) {
            DB::table('filmes_diretores')->insert([
                'id_filme' => $filmesIds[array_rand($filmesIds)],
                'id_diretor' => $diretoresIds[array_rand($diretoresIds)]
            ]);
        }
    }
}

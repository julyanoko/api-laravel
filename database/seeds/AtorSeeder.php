<?php

use Illuminate\Database\Seeder;

class AtorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Ator::class, 100)->create();
    }
}

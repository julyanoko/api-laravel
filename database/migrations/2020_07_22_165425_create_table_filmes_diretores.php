<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableFilmesDiretores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filmes_diretores', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_filme')->unsigned();
            $table->bigInteger('id_diretor')->unsigned();
            $table->foreign('id_filme')->references('id')->on('filmes')->onDelete('cascade');
            $table->foreign('id_diretor')->references('id')->on('diretores')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filmes_diretores');
    }
}

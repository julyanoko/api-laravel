<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableFilmesAtores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('filmes_atores', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_filme')->unsigned();
            $table->bigInteger('id_ator')->unsigned();
            $table->foreign('id_filme')->references('id')->on('filmes')->onDelete('cascade');
            $table->foreign('id_ator')->references('id')->on('atores')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('filmes_atores');
    }
}

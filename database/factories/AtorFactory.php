<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Ator::class, function (Faker $faker) {
    return [
        'nome' => $faker->name,
        'idade' => $faker->randomNumber(2)
    ];
});

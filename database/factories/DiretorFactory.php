<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Diretor::class, function (Faker $faker) {
    return [
        'nome' => $faker->name,
        'idade' => $faker->randomNumber(2)
    ];
});

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Filme::class, function (Faker $faker) {
    return [
        'titulo' => $faker->words(7, true),
        'sinopse' => $faker->text,
        'ano' => $faker->randomNumber(4)
    ];
});

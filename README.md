# API Laravel By Julyano

API criada em Laravel versão 7.21 contendo cadastros de:

  - Filmes;
  - Atores;
  - Diretores.

### Requisitos Mínimos:

  - Git;
  - Laravel Homestead OU as tecnologias abaixo:
  - Composer;
  - PHP >= 7.2.5;
  - Extensão PHP BCMath;
  - Extensão PHP Ctype;
  - Extensão PHP Fileinfo;
  - Extensão PHP JSON;
  - Extensão PHP Mbstring;
  - Extensão PHP OpenSSL;
  - Extensão PHP PDO;
  - Extensão PHP Tokenizer;
  - Extensão PHP XML.

### Instalação

Em um repositório de sua preferência, faça um clone do repositório git do projeto:

```sh
$ git clone https://julyanoko@bitbucket.org/julyanoko/api-laravel.git
```

Através do prompt de comando (Windows) ou terminal (Linux), navegue até a pasta do projeto.

Instale as dependencias do projeto executando o seguinte comando:

```sh
$ composer install
```

Após, realize a criação do banco de dados no gerenciador de banco de dados:
```sql
$ CREATE DATABASE `api-laravel` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
```

Agora, copie o arquivo .env.example criando o arquivo .env e altere as credenciais e nome do banco de dados criado:
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=api-laravel
DB_USERNAME=root
DB_PASSWORD=
```

Então, realize a crianção das tabelas pelo prompt de comando/terminal, dentro da pasta do projeto:
```sh
$ php artisan migrate
```

E para popular as tabelas criadas:
```sh
$ php artisan db:seed
```

Após isso, seu projeto está apto à rodar, utilizando o seguinte comando (através desse comando é possível ver qual a URL que o projeto estará rodando):
```sh
$ php artisan serve
```

### Configuração do Postman

Após importar a collection do Postman (arquivo collection-postman.json), caso haja necessidade em alterar a URL base do projeto, editar a Collection, em Variables, editar a variável url para a nova URL.

### Tecnologias utilizadas no projeto

Como não havia necessidade de tecnologias extras, foram utilizadas apenas as próprias ferramentas incluídas no laravel e nos requisitos mínimos:

  - Composer;
  - PHP >= 7.2.5;
  - Artisan:
    - make;
    - migrate;
    - db.

### Estratégias de desenvolvimento

O primeiro passo foi remover alguns arquivos gerados automaticamente na criação do projeto (referente à users). Depois, mapear as tabelas, apontando todas as colunas. Após, criar as migrations e indicar todas as colunas mapenadas para cada tabela. Então, criar as Models, Seeders e Factories para poder popular as tabelas com dados randômicos. E por último, criar as Controllers e suas devidas ações para cada classe, além de suas respectivas rotas, apontadas no arquivo api.php.
<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Api')->name('api.')->group(function () {
    Route::prefix('/filmes')->group(function () {
        Route::get('/', 'FilmeController@getAll');
        Route::get('/{id}', 'FilmeController@getOne');
        Route::put('/', 'FilmeController@create');
        Route::post('/{id}', 'FilmeController@update');
        Route::delete('/{id}', 'FilmeController@delete');
        Route::get('/{id}/atores', 'FilmeController@atores');
        Route::get('/{id}/diretores', 'FilmeController@diretores');
        Route::post('/{id}/atores/{id_ator}', 'FilmeController@insertAtor');
        Route::post('/{id}/diretores/{id_diretor}', 'FilmeController@insertDiretor');
        
    });

    Route::prefix('/atores')->group(function () {
        Route::get('/', 'AtorController@getAll');
        Route::get('/{id}', 'AtorController@getOne');
        Route::put('/', 'AtorController@create');
        Route::post('/{id}', 'AtorController@update');
        Route::delete('/{id}', 'AtorController@delete');
        Route::get('/{id}/filmes', 'AtorController@filmes');
    });

    Route::prefix('/diretores')->group(function () {
        Route::get('/', 'DiretorController@getAll');
        Route::get('/{id}', 'DiretorController@getOne');
        Route::put('/', 'DiretorController@create');
        Route::post('/{id}', 'DiretorController@update');
        Route::delete('/{id}', 'DiretorController@delete');
        Route::get('/{id}/filmes', 'DiretorController@filmes');
    });
});
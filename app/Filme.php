<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Filme extends Model
{
    protected $fillable = [
        'titulo', 'sinopse', 'ano'
    ];

    protected $table = 'filmes';

    public function Atores()
    {
        return $this->belongsToMany(Ator::class, 'filmes_atores', 'id_filme', 'id_ator');
    }

    public function Diretores()
    {
        return $this->belongsToMany(Diretor::class, 'filmes_diretores', 'id_filme', 'id_diretor');
    }
}

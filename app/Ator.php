<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ator extends Model
{
    protected $fillable = [
        'nome', 'idade'
    ];

    protected $table = 'atores';

    public function Filmes()
    {
        return $this->belongsToMany(Filme::class, 'filmes_atores', 'id_ator', 'id_filme');
    }
}

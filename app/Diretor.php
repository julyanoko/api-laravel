<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diretor extends Model
{
    protected $fillable = [
        'nome', 'idade'
    ];

    protected $table = 'diretores';

    public function Filmes()
    {
        return $this->belongsToMany(Filme::class, 'filmes_diretores', 'id_diretor', 'id_filme');
    }
}

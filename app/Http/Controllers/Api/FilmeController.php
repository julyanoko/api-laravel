<?php

namespace App\Http\Controllers\Api;

use App\API\ApiError;
use App\Filme;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FilmeController extends Controller
{

    private $filme;

    public function __construct(Filme $filme)
    {
        $this->filme = $filme;
    }

    public function getAll()
    {
        return response()->json($this->filme->paginate(20));
    }

    public function getOne($id)
    {
        $filme = $this->filme->find($id);
        if(!$filme) return response()->json(['msg' => 'Filme não encontrado!'], 404);
        return response()->json($filme);
    }

    public function create(Request $request)
    {
        try {
            $this->filme->create($request->all());
            return response()->json(['msg' => 'Filme cadastrado com sucesso!'], 201);
        } catch (\Exception $e) {
            if(config('app.debug')){
                return response()->json(ApiError::errorMessage($e->getMessage(), 422), 500);
            }
            return response()->json(ApiError::errorMessage('Houve um erro ao realizar a operação!', 422), 500);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $filme = $this->filme->find($id);
            if(!$filme) return response()->json(['msg' => 'Filme não encontrado!'], 404);
            $filme->update($request->all());
            return response()->json(['msg' => 'Filme atualizado com sucesso!'], 200);
        } catch (\Exception $e) {
            if(config('app.debug')){
                return response()->json(ApiError::errorMessage($e->getMessage(), 422), 500);
            }
            return response()->json(ApiError::errorMessage('Houve um erro ao realizar a operação!', 422), 500);
        }
    }

    public function delete($id)
    {
        try {
            $filme = $this->filme->find($id);
            if(!$filme) return response()->json(['msg' => 'Filme não encontrado!'], 404);
            $this->filme->destroy($id);
            return response()->json(['msg' => 'Filme excluído com sucesso!'], 200);
        } catch (\Exception $e) {
            if(config('app.debug')){
                return response()->json(ApiError::errorMessage($e->getMessage(), 422), 500);
            }
            return response()->json(ApiError::errorMessage('Houve um erro ao realizar a operação!', 422), 500);
        }
    }

    public function atores($id)
    {
        try {
            $filme = $this->filme->find($id);
            if(!$filme) return response()->json(['msg' => 'Filme não encontrado!'], 404);
            $atores = $filme->Atores;
            return response()->json($atores);
        } catch (\Exception $e) {
            if(config('app.debug')){
                return response()->json(ApiError::errorMessage($e->getMessage(), 422), 500);
            }
            return response()->json(ApiError::errorMessage('Houve um erro ao realizar a operação!', 422), 500);
        }
    }

    public function diretores($id)
    {
        try {
            $filme = $this->filme->find($id);
            if(!$filme) return response()->json(['msg' => 'Filme não encontrado!'], 404);
            $diretores = $filme->Diretores;
            return response()->json($diretores);
        } catch (\Exception $e) {
            if(config('app.debug')){
                return response()->json(ApiError::errorMessage($e->getMessage(), 422), 500);
            }
            return response()->json(ApiError::errorMessage('Houve um erro ao realizar a operação!', 422), 500);
        }
    }

    public function insertAtor($id, $id_ator)
    {
        try {
            $filme = $this->filme->find($id);
            if(!$filme) return response()->json(['msg' => 'Filme não encontrado!'], 404);
            $ator = \App\Ator::where('id', $id_ator)->first();
            if(!$ator) return response()->json(['msg' => 'Ator não encontrado!'], 404);
            if($filme->Atores()->find($id_ator)) return response()->json(['msg' => 'Esse ator já faz parte desse filme'], 422);
            $filme->Atores()->attach($id_ator);
            $filme->save();
            return response()->json(['msg' => 'Ator inserido no filme com sucesso!'], 200);
        } catch (\Exception $e) {
            if(config('app.debug')){
                return response()->json(ApiError::errorMessage($e->getMessage(), 422), 500);
            }
            return response()->json(ApiError::errorMessage('Houve um erro ao realizar a operação!', 422), 500);
        }
    }

    public function insertDiretor($id, $id_diretor)
    {
        try {
            $filme = $this->filme->find($id);
            if(!$filme) return response()->json(['msg' => 'Filme não encontrado!'], 404);
            $diretor = \App\Diretor::where('id', $id_diretor)->first();
            if(!$diretor) return response()->json(['msg' => 'Diretor não encontrado!'], 404);
            if($filme->Diretores()->find($id_diretor)) return response()->json(['msg' => 'Esse diretor já faz parte desse filme'], 422);
            $filme->Diretores()->attach($id_diretor);
            $filme->save();
            return response()->json(['msg' => 'Diretor inserido no filme com sucesso!'], 200);
        } catch (\Exception $e) {
            if(config('app.debug')){
                return response()->json(ApiError::errorMessage($e->getMessage(), 422), 500);
            }
            return response()->json(ApiError::errorMessage('Houve um erro ao realizar a operação!', 422), 500);
        }
    }
}

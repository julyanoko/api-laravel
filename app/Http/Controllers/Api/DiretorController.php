<?php

namespace App\Http\Controllers\Api;

use App\API\ApiError;
use App\Diretor;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DiretorController extends Controller
{
    private $diretor;

    public function __construct(Diretor $diretor)
    {
        $this->diretor = $diretor;
    }

    public function getAll()
    {
        return response()->json($this->diretor->paginate(20));
    }

    public function getOne($id)
    {
        $diretor = $this->diretor->find($id);
        if(!$diretor) return response()->json(['msg' => 'Diretor não encontrado!'], 404);
        return response()->json($diretor);
    }

    public function create(Request $request)
    {
        try {
            $this->diretor->create($request->all());
            return response()->json(['msg' => 'Diretor cadastrado com sucesso!'], 201);
        } catch (\Exception $e) {
            if(config('app.debug')){
                return response()->json(ApiError::errorMessage($e->getMessage(), 422), 500);
            }
            return response()->json(ApiError::errorMessage('Houve um erro ao realizar a operação!', 422), 500);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $diretor = $this->diretor->find($id);
            if(!$diretor) return response()->json(['msg' => 'Diretor não encontrado!'], 404);
            $diretor->update($request->all());
            return response()->json(['msg' => 'Diretor atualizado com sucesso!'], 200);
        } catch (\Exception $e) {
            if(config('app.debug')){
                return response()->json(ApiError::errorMessage($e->getMessage(), 422), 500);
            }
            return response()->json(ApiError::errorMessage('Houve um erro ao realizar a operação!', 422), 500);
        }
    }

    public function delete($id)
    {
        try {
            $diretor = $this->diretor->find($id);
            if(!$diretor) return response()->json(['msg' => 'Diretor não encontrado!'], 404);
            $this->diretor->destroy($id);
            return response()->json(['msg' => 'Diretor excluído com sucesso!'], 200);
        } catch (\Exception $e) {
            if(config('app.debug')){
                return response()->json(ApiError::errorMessage($e->getMessage(), 422), 500);
            }
            return response()->json(ApiError::errorMessage('Houve um erro ao realizar a operação!', 422), 500);
        }
    }

    public function filmes($id)
    {
        try {
            $diretor = $this->diretor->find($id);
            if(!$diretor) return response()->json(['msg' => 'Diretor não encontrado!'], 404);
            $filmes = $diretor->Filmes;
            return response()->json($filmes);
        } catch (\Exception $e) {
            if(config('app.debug')){
                return response()->json(ApiError::errorMessage($e->getMessage(), 422), 500);
            }
            return response()->json(ApiError::errorMessage('Houve um erro ao realizar a operação!', 422), 500);
        }
    }
}

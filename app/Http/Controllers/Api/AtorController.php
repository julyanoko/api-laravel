<?php

namespace App\Http\Controllers\Api;

use App\API\ApiError;
use App\Ator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AtorController extends Controller
{
    private $ator;

    public function __construct(Ator $ator)
    {
        $this->ator = $ator;
    }

    public function getAll()
    {
        return response()->json($this->ator->paginate(20));
    }

    public function getOne($id)
    {
        $ator = $this->ator->find($id);
        if(!$ator) return response()->json(['msg' => 'Ator não encontrado!'], 404);
        return response()->json($ator);
    }

    public function create(Request $request)
    {
        try {
            $this->ator->create($request->all());
            return response()->json(['msg' => 'Ator cadastrado com sucesso!'], 201);
        } catch (\Exception $e) {
            if(config('app.debug')){
                return response()->json(ApiError::errorMessage($e->getMessage(), 422), 500);
            }
            return response()->json(ApiError::errorMessage('Houve um erro ao realizar a operação!', 422), 500);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $ator = $this->ator->find($id);
            if(!$ator) return response()->json(['msg' => 'Ator não encontrado!'], 404);
            $ator->update($request->all());
            return response()->json(['msg' => 'Ator atualizado com sucesso!'], 200);
        } catch (\Exception $e) {
            if(config('app.debug')){
                return response()->json(ApiError::errorMessage($e->getMessage(), 422), 500);
            }
            return response()->json(ApiError::errorMessage('Houve um erro ao realizar a operação!', 422), 500);
        }
    }

    public function delete($id)
    {
        try {
            $ator = $this->ator->find($id);
            if(!$ator) return response()->json(['msg' => 'Ator não encontrado!'], 404);
            $this->ator->destroy($id);
            return response()->json(['msg' => 'Ator excluído com sucesso!'], 200);
        } catch (\Exception $e) {
            if(config('app.debug')){
                return response()->json(ApiError::errorMessage($e->getMessage(), 422), 500);
            }
            return response()->json(ApiError::errorMessage('Houve um erro ao realizar a operação!', 422), 500);
        }
    }

    public function filmes($id)
    {
        try {
            $ator = $this->ator->find($id);
            if(!$ator) return response()->json(['msg' => 'Ator não encontrado!'], 404);
            $filmes = $ator->Filmes;
            return response()->json($filmes);
        } catch (\Exception $e) {
            if(config('app.debug')){
                return response()->json(ApiError::errorMessage($e->getMessage(), 422), 500);
            }
            return response()->json(ApiError::errorMessage('Houve um erro ao realizar a operação!', 422), 500);
        }
    }
}
